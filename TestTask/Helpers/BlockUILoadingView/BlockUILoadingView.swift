//
//  BlockUILoadingView.swift
//
//
//
//  Copyright © 2020 Lampa. All rights reserved.
//

import UIKit

class BlockUILoadingView: UIView {
    
    @IBOutlet weak var backgroundContainerView: UIView!
    @IBOutlet weak var kdView: UIView!
    @IBOutlet weak var activityIndicator: UIActivityIndicatorView!
    
    private let duration: Double = 0.25
    private let opacity: CGFloat = 0.4
    private let scale: CGFloat = 0.8
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        showBackGround()
        setupView()
    }
    
    class func fromNib() -> BlockUILoadingView {
        return UINib(nibName: String(describing: self), bundle: nil).instantiate(withOwner: nil, options: nil)[0] as! BlockUILoadingView
    }
    
    func setupView() {
        let transform: CGAffineTransform = CGAffineTransform(scaleX: scale, y: scale)
        activityIndicator.transform = transform
    }
    
    func showBackGround() {
        UIView.animate(withDuration: duration, animations: {
            self.backgroundContainerView.alpha = self.opacity
        })
    }
    
    func dismissView() {
        UIView.animate(withDuration: duration, animations: {
            self.backgroundContainerView.alpha = 0
        }) { (_) in
            self.removeFromSuperview()
        }
    }
}
