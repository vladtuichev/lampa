//
//  RestClient.swift
//
//
//  Created by Lampa on 29.06.2020.
//  Copyright © 2020 Lampa. All rights reserved.
//

import Foundation
import Alamofire
import ObjectMapper
import SwiftyJSON

class RestClient: NSObject {
    internal var http = HttpService()
    internal let baseUrl = ApiSettings.shared.kServerBaseURL
    
    let dataIsNil = CustomError.init(localizedDescription: StringValues.Base.kOopsTryAgainLater, code: 404)
    
    internal func cancellRequests() {
        http.cancellAllRequests()
    }
    
    internal func response<P: BaseMappable>(_ response: Any?, _ error: Error?, modelCls: P.Type, key: String? = nil, resp: @escaping IdResponseBlock) {
        if let err = error {
            return resp(nil, err)
        }
        
        guard let data = response else {
            return resp(nil, error)
        }
        
        var json = JSON(data)
        
        if let jsonKey = key {
            json = JSON(data)[jsonKey]
        }
        
        self.parseData(object: json.rawValue,
                       modelCls: modelCls.self,
                       response: resp)
    }
    
    private func parseData<P: BaseMappable>(object: Any, modelCls: P.Type, response: (IdResponseBlock)) {
        
        if object is NSArray {
            let result = Mapper<P>().mapArray(JSONObject: object)
            return response(result, nil)
        }
        
        if object is NSDictionary {
            let model = Mapper<P>().map(JSONObject: object)!
            return response(model, nil)
        }
    }
}
