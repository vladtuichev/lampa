//
//  ApiSettings.swift
//  
//
//  Created by Lampa on 29.06.2020.
//  Copyright © 2019 Lampa. All rights reserved.
//

import Foundation

class ApiSettings {
    
    static let shared = ApiSettings()
    
    private init() {}
    
    let kServerBaseURL = "https://api.themoviedb.org/3/"
    private let kServerKey = "f910e2224b142497cc05444043cc8aa4"
    
    private var currentDefaults: UserDefaults = .standard
        
    var token: String {
        return kServerKey
    }
}
