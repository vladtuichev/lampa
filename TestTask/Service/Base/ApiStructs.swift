//
//  ApiStructs.swift
//
//
//  Created by Lampa on 29.06.2020.
//  Copyright © 2019 Lampa. All rights reserved.
//

import Foundation

struct Keys {
    static let token = "api_key"
}

struct Requests {
    enum Movies: String {
        case topRated = "movie/top_rated"
    }
}
