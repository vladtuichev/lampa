//
//  MovieRequests.swift
//  TestTask
//
//  Created by Lampa on 29.06.2020.
//  Copyright © 2020 Lampa. All rights reserved.
//

import Foundation

class MoviesRequests: RestClient {
    func getTopRated(resp: @escaping IdResponseBlock) {
        let url = baseUrl + Requests.Movies.topRated.rawValue
        
        http.queryBy(url, method: .get, queue: .defaultQos, resp: { (response, error) in
            self.response(response, error, modelCls: MoviesPageEntity.self, resp: resp)
        })
    }
}
