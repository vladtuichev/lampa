//
//  BaseProtocol.swift
//  TestTask
//
//  Created by Lampa on 29.06.2020.
//  Copyright © 2020 Lampa. All rights reserved.
//

import Foundation

protocol BaseViewControllerProtocol: class {
    func showErrorAlert(message: String)
    func inBlockScreenViewStart(flag: Bool)
    func blockScreenViewStart(flag: Bool)
}
