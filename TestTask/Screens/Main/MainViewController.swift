//
//  ViewController.swift
//  TestTask
//
//  Created by Lampa on 29.06.2020.
//  Copyright © 2020 Lampa. All rights reserved.
//

import UIKit

protocol MainViewControllerProtocol: BaseViewControllerProtocol {
    func reloadTable()
}

class MainViewController: UIViewController {
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var noResultsView: UIView!
    
    private var presenter: MainPresenterProtocol!
    private let rowHeight: CGFloat = 60
    private let headerHeight: CGFloat = 80
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.presenter = MainPresenter(view: self)
        setupViews()
    }
    
    private func setupViews() {
        self.setupTableView()
        self.noResultsView.isHidden = true
    }
    
    private func setupTableView() {
        self.tableView.registerCell(MainTableViewCell.self)
        self.tableView.dataSource = self
        self.tableView.delegate = self
    }
}

extension MainViewController: UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.presenter.getNumberOfRows()
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = self.tableView.createCell(MainTableViewCell.self, indexPath)
        self.presenter.configurateCell(cell: cell, path: indexPath)
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let tableHeader: MainTableHeader = .fromNib()
        
        tableHeader.sortHandler = { [weak self] in
            self?.presenter.sortAction()
        }
        
        tableHeader.removeHandler = { [weak self] in
            self?.presenter.removeAction()
        }
        
        return tableHeader
    }
}

extension MainViewController: UITableViewDelegate {
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return self.rowHeight
    }
    
    func tableView(_ tableView: UITableView, estimatedHeightForRowAt indexPath: IndexPath) -> CGFloat {
        return self.rowHeight
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return self.headerHeight
    }
    
    func tableView(_ tableView: UITableView, estimatedHeightForHeaderInSection section: Int) -> CGFloat {
        return self.headerHeight
    }
    
    ///remove by swipe action
    func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
        return true
    }
    
    func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCell.EditingStyle, forRowAt indexPath: IndexPath) {
        self.tableView.beginUpdates()
        
        switch editingStyle {
        case .delete:
            self.presenter.removeCell(path: indexPath)
            self.tableView.deleteRows(at: [indexPath], with: .fade)
            
        default: break
        }
        
        self.tableView.endUpdates()
    }
}

extension MainViewController: MainViewControllerProtocol {
    func reloadTable() {
        DispatchQueue.main.async {
            self.noResultsView.isHidden = self.presenter.getNumberOfRows() > 0
            self.tableView.reloadTable(isAnimate: true)
        }
    }
}
