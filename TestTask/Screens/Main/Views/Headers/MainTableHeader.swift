//
//  MainTableHeader.swift
//  TestTask
//
//  Created by Lampa on 29.06.2020.
//  Copyright © 2020 Lampa. All rights reserved.
//

import UIKit

class MainTableHeader: UIView {
    var sortHandler: (() -> Void)?
    var removeHandler: (() -> Void)?
    
    @IBAction func sortButtonClicked(_ sender: UIButton) {
        self.sortHandler?()
    }
    
    @IBAction func removeButtonClicked(_ sender: UIButton) {
        self.removeHandler?()
    }
}
