//
//  MainTableViewCell.swift
//  TestTask
//
//  Created by Lampa on 29.06.2020.
//  Copyright © 2020 Lampa. All rights reserved.
//

import UIKit
import SDWebImage

protocol MainTableViewCellProtocol {
    func display(title: String?)
    func display(rating: Double?)
    func display(image: String?)
}

class MainTableViewCell: UITableViewCell {
    @IBOutlet weak var posterImage: UIImageView!
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var ratingLabel: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        self.resetContent()
    }
    
    override func prepareForReuse() {
        super.prepareForReuse()
        
        self.resetContent()
    }
    
    private func resetContent() {
        self.posterImage.sd_cancelCurrentImageLoad()
        self.posterImage.image = nil
        
        self.titleLabel.text = nil
        self.ratingLabel.text = nil
    }
}

extension MainTableViewCell: MainTableViewCellProtocol {
    func display(title: String?) {
        self.titleLabel.text = title
    }
    
    func display(rating: Double?) {
        if let rating = rating {
            self.ratingLabel.text = String(rating)
        }
    }
    
    func display(image: String?) {
        if let image = image {
            self.posterImage.sd_setImage(with: URL(string: image), completed: nil)
        }
    }
}
