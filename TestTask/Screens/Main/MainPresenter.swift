//
//  MainPresenter.swift
//  TestTask
//
//  Created by Lampa on 29.06.2020.
//  Copyright © 2020 Lampa. All rights reserved.
//

import Foundation

protocol MainPresenterProtocol {
    init(view: MainViewControllerProtocol)
    func configurateCell(cell: MainTableViewCellProtocol, path: IndexPath)
    func getNumberOfRows() -> Int
    func sortAction()
    func removeAction()
    func removeCell(path: IndexPath)
}

class MainPresenter: MainPresenterProtocol {
    private unowned let view: MainViewControllerProtocol!
    private let moviewRequest = MoviesRequests()
    private var data = MoviesPageEntity()
    
    required init(view: MainViewControllerProtocol) {
        self.view = view
        self.loadData()
    }
    
    func configurateCell(cell: MainTableViewCellProtocol, path: IndexPath) {
        let image = data?.result?[safe: path.item]?.posterPath
        let title = data?.result?[safe: path.item]?.title
        let rating = data?.result?[safe: path.item]?.voteAverage
        
        cell.display(image: image)
        cell.display(title: title)
        cell.display(rating: rating)
    }
    
    func getNumberOfRows() -> Int {
        return self.data?.result?.count ?? 0
    }
    
    func removeCell(path: IndexPath) {
        self.data?.result?.remove(at: path.row)
    }
    
    func sortAction() {
        self.data?.result?.sort(by: { ($0.title ?? "") < ($1.title ?? "") })
        self.view.reloadTable()
    }
    
    func removeAction() {
        self.data?.result?.removeAll(where: { $0.title?.first?.lowercased() != "c" })
        self.view.reloadTable()
    }
    
    private func loadData() {
        self.view.blockScreenViewStart(flag: true)
        
        self.moviewRequest.getTopRated() { [weak self] result, error in
            guard let `self` = self else { return }
            
            self.view.blockScreenViewStart(flag: false)
            
            if let err = error {
                self.view.showErrorAlert(message: err.localizedDescription)
                return
            }
            
            guard let movies = result as? MoviesPageEntity else { return }
            
            self.data = movies
            self.view.reloadTable()
        }
    }
}
