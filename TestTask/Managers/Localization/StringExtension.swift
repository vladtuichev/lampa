//
//  StringExtension.swift
//  TestTask
//
//  Created by Lampa on 29.06.2020.
//  Copyright © 2020 Lampa. All rights reserved.
//

import Foundation

extension String {
    var localized: String {
        let localizationKey = LocalManager.shared.localizationKey
        
        if let _ = UserDefaults.standard.string(forKey: localizationKey) {} else {
            
            let languageStr = Language.english.rawValue
            
            UserDefaults.standard.set(languageStr, forKey: localizationKey)
        }
        
        let lang = UserDefaults.standard.string(forKey: localizationKey) ?? ""
        
        guard let path = Bundle.main.path(forResource: lang, ofType: "lproj") else { return self }
        guard let bundle = Bundle(path: path) else { return self }
        
        var result = NSLocalizedString(self, tableName: nil, bundle: bundle, value: "", comment: "")
        
        if result.isEmpty { result = self }
        
        return result
    }
}
