//
//  LocalManager.swift
//  TestTask
//
//  Created by Lampa on 29.06.2020.
//  Copyright © 2020 Lampa. All rights reserved.
//

import Foundation

private let appleLanguageKey = "AppleLanguages"

enum Language: String {
    case english = "en"
    case ukrainian = "uk"
    case russian = "ru"
    
    var languageName: String {
        switch self {
        case .english: return "English"
        case .ukrainian: return "Українська"
        case .russian: return "Русский"
        }
    }
}

class LocalManager: NSObject {
    static let shared = LocalManager()
    let localizationKey = "i18n_language"

    func currentAppleLanguage() -> String {
        let langArray = UserDefaults.standard.object(forKey: appleLanguageKey) as! NSArray
        guard let current = langArray.firstObject as? String else { return "" }
        let endIndex = current.startIndex
        let currentWithoutLocale = String(current[..<current.index(endIndex, offsetBy: 2)])
        
        return currentWithoutLocale
    }
    
    func setAppleLanguageTo(lang: Language) {
        UserDefaults.standard.set([lang.rawValue, currentAppleLanguage()], forKey: appleLanguageKey)
        UserDefaults.standard.set(lang.rawValue, forKey: localizationKey)
    }
}
