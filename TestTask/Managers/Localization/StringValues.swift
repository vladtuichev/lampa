//
//  StringValues.swift
//  TestTask
//
//  Created by Lampa on 29.06.2020.
//  Copyright © 2020 Lampa. All rights reserved.
//

import Foundation

struct StringValues {
    struct Base {
        static let kNoInternetConnection = "No internet connection"
        static let kInternetRestored = "Internet restored"
        static let kNotAccessToServer = "Failed access to server"
        static let kOopsTryAgainLater = "Oops, something went wrong. Please try again later."
        static let kOk = "Ok"
        static let kCancel = "Cancel"
        static let kErrorAlertTitle = "Error"
        static let kYes = "Yas"
        static let kNo = "No"
    }
}
