//
//  MoviesPageEntity.swift
//  TestTask
//
//  Created by Lampa on 29.06.2020.
//  Copyright © 2020 Lampa. All rights reserved.
//

import Foundation
import ObjectMapper

class MoviesPageEntity: Mappable {
    var page: Int?
    var totalResults: Int?
    var totalPages: Int?
    var result: [MovieEntity]?
    
    required init?(map: Map) {}
    required init?() {}
    
    func mapping(map: Map) {
        page           <- map ["page"]
        totalResults   <- map ["total_results"]
        totalPages     <- map ["total_pages"]
        result         <- map ["results"]
    }
}
