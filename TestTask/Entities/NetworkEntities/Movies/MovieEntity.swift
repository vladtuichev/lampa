//
//  MovieEntity.swift
//  TestTask
//
//  Created by Lampa on 29.06.2020.
//  Copyright © 2020 Lampa. All rights reserved.
//

import Foundation
import ObjectMapper

class MovieEntity: Mappable {
    private let posterBaseUrl = "https://image.tmdb.org/t/p/w600_and_h900_bestv2"
    private let backdropBaseUrl = "https://image.tmdb.org/t/p/w533_and_h300_bestv2"
    
    var popularity: Double?
    var voteCount: Int?
    var video: Bool?
    var posterPath: String?
    var id: Int?
    var adult: Bool?
    var backdropPath: String?
    var originalLanguage: String?
    var originalTitle: String?
    var genreIds: [Int]?
    var title: String?
    var voteAverage: Double?
    var overview: String?
    var releaseDate: String?
    
    required init?(map: Map) {}
    
    func mapping(map: Map) {
        
        if let value: String = map["poster_path"].value() {
            posterPath = posterBaseUrl + value
        }
        
        if let value: String = map["backdrop_path"].value() {
            backdropPath = backdropBaseUrl + value
        }
        
        id                  <- map ["id"]
        adult               <- map ["adult"]
        popularity          <- map ["popularity"]
        voteCount           <- map ["vote_count"]
        video               <- map ["video"]
        originalLanguage    <- map ["original_language"]
        originalTitle       <- map ["original_title"]
        genreIds            <- map ["genre_ids"]
        title               <- map ["title"]
        voteAverage         <- map ["vote_average"]
        overview            <- map ["overview"]
        releaseDate         <- map ["release_date"]
    }
}
